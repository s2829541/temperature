package nl.utwente.di.bookQuote;

public class Converter {
    Double celsiusToFahrenheit(String degrees) throws NumberFormatException {
        return Double.parseDouble(degrees) * 1.8 + 32;
    }
}
