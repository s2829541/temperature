package nl.utwente.di.bookQuote;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Example of a Servlet that gets an ISBN number and returns the book price
 */

public class CelsiusToFahrenheit extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Converter converter;

    public void init() throws ServletException {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius to Fahrenheit";
        String degreesFahrenheit;
        String text;
        try {
            degreesFahrenheit = String.valueOf(converter.celsiusToFahrenheit(request.getParameter("isbn")));
            text = "  <P>Celsius: " +
                    request.getParameter("isbn") + "\n" +
                    "  <P>Fahrenheit: " +
                    degreesFahrenheit +
                    "</BODY></HTML>";
        } catch (NumberFormatException e) {
            text = "  <P>That is not valid input!" +
                    "</BODY></HTML>";
        }

        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" + text);
    }


}
